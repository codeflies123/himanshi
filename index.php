<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8">
	<title>Codeflies Payslip</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="icon" href="logo.png" type="image/png" />
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>
<div class="main-sec">
	<div class="table_class">
		<form method="post" action="pdf_generate.php">
			<table class="table_first table" cellpadding="0" cellspacing="0">
				<tr class="fir_st_tr">
				    <td colspan="4"><h1 style="margin:0;"><span>Payslip</span> <input type="text" name="month" id="month" placeholder="Enter Month"></h1></td>
				    <td colspan="2"><img class="logo" src="logo.png" style="height:20px;float:right;"></td> 
				  </tr>		    
				<tr>
				<tr>
					<td colspan="6"><input type="text" name="name" id="name" placeholder="Enter Name Of Employee"></td>
				</tr>
				<tr class="blue">
					<th colspan="2">Employee Details</th>
					<th colspan="2">Payment & Leave Details</th>
					<th colspan="2">Location Details</th>
				</tr>
				<tr>
					<td><strong>Emp No.</strong></td>
					<td><input type="text" name="emp_no" id="emp_no" placeholder="Enter Employee Id"></td>
					<td><strong> Bank Name</strong></td>
					<td><input type="text" name="bank_name" id="bank_name" placeholder="Enter Bank Name"></td>
					<td><strong>Location</strong></td>
					<td><input type="text" name="location" id="location" placeholder="Enter Location"></td>
				</tr>
				<tr>
					<td><strong>Dsgn.</strong></td>
					<td><input type="text" name="dsgn" id="dsgn" placeholder="Enter Designation"></td>
					<td><strong>Acc No.</strong></td>
					<td><input type="number" name="acc_no" id="acc_no" placeholder="Enter Account Number"></td>
					<td><strong>Location</strong></td>
					<td><input type="text" name="location1" id="location1" placeholder="Enter Location"></td>
				</tr>
				<tr>
					<td><strong>Grade</strong></td>
					<td><input type="grade" name="grade" id="grade" placeholder="Enter Grade"></td>
					<td><strong>Days paid</strong></td>
					<td><input type="number" step="0.01" name="days_paid" id="days_paid" placeholder="Enter days to be paid"></td>
					<td><strong>Depute Br.</strong></td>
					<td><input type="text" name="location2" id="location2" placeholder="Enter Location"></td>
				</tr>
				<tr>
					<td><strong>PAN</strong></td>
					<td><input type="text" name="pan" id="pan" placeholder="Enter Pan Number"></td>
					<td><strong>Leave Balance</strong></td>
					<td class="top-tbl-tb-td">
						<table cellspacing="0" cellpadding="0" class="top-tbl-tb">
							<tr>
								<td>EL</td>
								<td><input type="number" step="0.01" name="el" id="el" placeholder="0.8"></td>
								<td>SL</td>
								<td><input type="number" step="0.01" name="sl" id="sl" placeholder="0.4"></td>
								<td>CL</td>
								<td><input type="number" step="0.01" name="cl" id="cl" placeholder="0.7"></td>
							</tr>
						</table>
					</td>
					<td><strong>WON/SWON</strong></td>
					<td><input type="number" name="won" id="won" placeholder="2104100"></td>
				</tr>
			</table>

			<table class="table_seco mar_top table" cellpadding="0" cellspacing="0">
				<tr class="table_seco_tr">
					<td>
						<table class="table_seco_inn_table" cellpadding="0" cellspacing="0">
							<tr class="blue">
								<th>Earnings</th>
								<th>Arrears <small>(INR)</small></th>
								<th>Current <small>(INR)</small></th>
							</tr>
							<tr>
								<td>Basic Salary</td>
								<td></td>
								<td><input type="number" step="0.01" name="basic_sal" id="basic_sal" placeholder="58000.44"></td>
							</tr>
							<tr>
								<td>Conveyance Non Taxable</td>
								<td></td>
								<td><input type="number" step="0.01" name="conveyance" id="conveyance" placeholder="58000.44"></td>
							</tr>
							<tr>
								<td>Basic Salary</td>
								<td></td>
								<td><input type="number"  step="0.01" name="basic_sal1" id="basic_sal1" placeholder="58000.44"></td>
							</tr>
							
							<tr>
								<td>Variable Allowance</td>
								<td></td>
								<td><input type="number" step="0.01" name="var_allow" id="var_allow" placeholder="58000.44"></td>
							</tr>
							<tr>
								<td>Leave Travel Allowance</td>
								<td></td>
								<td><input type="number" step="0.01" name="travel_allow" id="travel_allow" placeholder="58000.44"></td>
							</tr>
							<tr>
								<td>Personal Allowance</td>
								<td></td>
								<td><input type="number" step="0.01" name="personal_allow" id="personal_allow" placeholder="58000.44"></td>
							</tr>
							<tr class="border_none">
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
							<tr class="border_none">
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
							<tr class="border_none">
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
							<tr class="border_none">
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
							<tr class="table_seco_inn_table_ftr">
								<td>Total Earnings <small>(Current + Arrears)</small></td>
								<td>&nbsp;</td>
								<td><input type="number" step="0.01" name="total_earning" id="total_earning" placeholder="58000.44"></td>
							</tr>
						</table>
					</td>
					<td>
						<table class="table_seco_inn_table" cellpadding="0" cellspacing="0">
							<tr class="blue">
								<th>Deductions</th>
								<th>Amount <small>(INR)</small></th>
							</tr>
							<tr>
								<td>Provident Fund</td>
								<td><input type="number" step="0.01" name="pf" id="pf" placeholder="5800.44"></td>
							</tr>
							<tr>
								<td>Professional Tax</td>
								<td><input type="number" step="0.01" name="pt" id="pt" placeholder="5800.44"></td>
							</tr>
							<tr>
								<td>Income Tax</td>
								<td><input type="number" step="0.01" name="it" id="it" placeholder="5800.44"></td>
							</tr>
							<tr class="border_none">
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
							<tr class="border_none">
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
							<tr class="border_none">
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
							<tr class="border_none">
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
							<tr class="border_none">
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
							<tr class="border_none">
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
							<tr class="border_none">
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
							<tr class="table_seco_inn_table_ftr">
								<td>Total Deductions</td>
								<td><input type="number" step="0.01" name="total_deduct" id="total_deduct" placeholder="8000.44"></td>
							</tr>

						</table>
					</td>
				</tr>
			</table>

			<table class="table_third mar_top table" cellpadding="0" cellspacing="0">
				<td>
					<table cellpadding="0" cellspacing="0">
						<tr>
							<td class="padding_10" colspan="2" style="background: #00a5b4;color: #fff;font-weight: bold;">Retirals<br>as on Month end</td>
							<td>
								<table cellpadding="0" cellspacing="0">
									<tr>
										<td style="background: #00a5b4;color: #fff;">Provident Fund*</td>
									</tr>
									<tr>
										<td><input type="number" step="0.01" name="pro_fund" id="pro_fund" placeholder="5000"></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
				<td style="width: 20%;"></td>
				<td>
					<table cellpadding="0" cellspacing="0">
						<tr>
							<td class="padding_10" colspan="2" style="font-weight: bold;background: #00a5b4;color: #fff;">Retirals<br>as on Month end</td>
							<td>
								<table cellpadding="0" cellspacing="0">
									<tr>
										<td style="background: #00a5b4;color: #fff;">Provident Fund*</td>
									</tr>
									<tr>
										<td><input type="number" step="0.01" name="pro_fund1" id="pro_fund1" placeholder="5800"></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</table>

			<table class="table_fourth table mar_top" cellpadding="0" cellspacing="0">
				<tr class="blue">
					<th colspan="4">Projected Annual Tax Information</th>
					<th colspan="2">Chapter VIA Relief</th>
				</tr>
				<tr>
					<td>Annual Income*</td>
					<td><input type="number" step="0.01" name="anu_income" id="anu_income" placeholder="5800"></td>
					<td> Net Tax Income r/o</td>
					<td><input type="number" step="0.01" name="tax_income" id="tax_income" placeholder="5800"></td>
					<td>80C-Max 1 Lac</td>
					<td><input type="number" step="0.01" name="max_income" id="max_income" placeholder="5800"></td>
				</tr>
				<tr>
					<td>Professional Tax</td>
					<td><input type="number" step="0.01" name="pro_tax" id="pro_tax" placeholder="5800"></td>
					<td>Total Tax Payable</td>
					<td><input type="number" step="0.01" name="total_tax" id="total_tax" placeholder="5800"></td>
					<td>80D</td>
					<td><input type="number" step="0.01" name="80d" id="80d" placeholder="5800"></td>
				</tr>
				<tr>
					<td>Chapter VIA relief</td>
					<td><input type="number" step="0.01" name="anu_income" id="anu_income" placeholder="5800"></td>
					<td>Tax Deducted till date</td>
					<td><input type="number" step="0.01" name="tax_deduct" id="tax_deduct" placeholder="5800"></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>Balance Tax</td>
					<td><input type="number" step="0.01" name="bal_tax" id="bal_tax" placeholder="5800"></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</table>

			<table class="table_five table mar_top" cellpadding="0" cellspacing="0">
				<tr class="blue">
					<th colspan="4">Investment Description</th>
					<th colspan="2">Exemption Considered*</th>
				</tr>
				<tr>
					<td style="width: 20%">80D-Medical Premium</td>
					<td style="width: 15%"><input type="number" step="0.01" name="medical_pre" id="medical_pre" placeholder="5800"></td>
					<td style="width: 15%">&nbsp;</td>
					<td style="width: 15%">&nbsp;</td>
					<td>Conveyance</td>
					<td><input type="number" step="0.01" name="convey" id="convey" placeholder="5800"></td>
				</tr>
				<tr>
					<td>PF Contribution</td>
					<td><input type="number" step="0.01" name="pf_countri" id="pf_countri" placeholder="5800"></td>
					<td style="width: 15%">&nbsp;</td>
					<td style="width: 15%">&nbsp;</td>
					<td>Rent</td>
					<td><input type="number" step="0.01" name="rent" id="rent" placeholder="5800"></td>
				</tr>
				<tr>
					<td colspan="6" style="border:0;font-size: 13px;margin-top: 10px;">* Please Note, Annual Income is after considering the above exemption - if any.</td>
				</tr>
				
				<tr class="last_column">
					<td class="td-submit"><input type="submit" name="submit" id="submit" value="Print"></td><td class="reset" colspan="4"></td>
					<td class="td-submit" id="reset"><a href="javascript:location.reload(true)">Reset</a></td>
				</tr>
			</table>
       </form>
	</div>
</div>


	<!-- 	<script type="text/javascript">
		// Reload page 5 seconds after form submission
		
			$(function(){
		    $('#submit').click(function() {
		        $("form").onsubmit = setTimeout(function () {
				        location.reload(true); //MODIFICATION
				    }, 1000);
		    	});
			});
		</script> -->
</body>
</html>