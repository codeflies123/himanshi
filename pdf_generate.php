<?php

require_once 'dompdf/autoload.inc.php';

use Dompdf\Dompdf;


/* instantiate and use the dompdf class */
$dompdf = new Dompdf();


$html = '<link rel="stylesheet" href="css/style.css">
       <div class="main-sec">
      
	   <div class="table_class">
	
		<table class="table_first table" cellpadding="0" cellspacing="0">
			<tr class="fir_st_tr">
			    <td colspan="4"><h1 style="margin:0;">Payslip '.$_POST['month'].'</h1></td>
			    <td colspan="2"><img class="logo" src="logo.png" style="height:20px;float:right;"></td> 
			  </tr>		    
			<tr>
				<td colspan="6">'.$_POST['name'].'</td>
			</tr>
			<tr class="blue">
				<th colspan="2">Employee Details</th>
				<th colspan="2">Payment & Leave Details</th>
				<th colspan="2">Location Details</th>
			</tr>
			<tr>
				<td><strong>Emp No.</strong></td>
				<td>'.$_POST['emp_no'].'</td>
				<td><strong> Bank Name</strong></td>
				<td>'.$_POST['bank_name'].'</td>
				<td><strong>Location</strong></td>
				<td>'.$_POST['location'].'</td>
			</tr>
			<tr>
				<td><strong>Dsgn.</strong></td>
				<td>'.$_POST['dsgn'].'</td>
				<td><strong>Acc No.</strong></td>
				<td>'.$_POST['acc_no'].'</td>
				<td><strong>Location</strong></td>
				<td>'.$_POST['location1'].'</td>
			</tr>
			<tr>
				<td><strong>Grade</strong></td>
				<td>'.$_POST['grade'].'</td>
				<td><strong>Days paid</strong></td>
				<td>'.$_POST['days_paid'].'</td>
				<td><strong>Depute Br.</strong></td>
				<td>'.$_POST['location2'].'</td>
			</tr>
			<tr>
				<td><strong>PAN</strong></td>
				<td>'.$_POST['pan'].'</td>
				<td><strong>Leave Balance</strong></td>
				<td class="top-tbl-tb-td">
					<table cellspacing="0" cellpadding="0" class="top-tbl-tb">
						<tr>
							<td style="height:37px;">EL</td>
							<td style="height:37px;">'.$_POST['el'].'</td>
							<td style="height:37px;">SL</td>
							<td style="height:37px;">'.$_POST['sl'].'</td>
							<td style="height:37px;">CL</td>
							<td style="height:37px;">'.$_POST['cl'].'</td>
						</tr>
					</table>
				</td>
				<td><strong>WON/SWON</strong></td>
				<td>'.$_POST['won'].'</td>
			</tr>
		</table>

		<table class="table_seco mar_top table" cellpadding="0" cellspacing="0">
			<tr class="table_seco_tr">
				<td>
					<table class="table_seco_inn_table" cellpadding="0" cellspacing="0">
						<tr class="blue">
							<th>Earnings</th>
							<th>Arrears <small>(INR)</small></th>
							<th>Current <small>(INR)</small></th>
						</tr>
						<tr>
							<td>Basic Salary</td>
							<td></td>
							<td>'.$_POST['basic_sal'].'</td>
						</tr>
						<tr>
							<td>Conveyance Non Taxable</td>
							<td></td>
							<td>'.$_POST['conveyance'].'</td>
						</tr>
						<tr>
							<td>Basic Salary</td>
							<td></td>
							<td>'.$_POST['basic_sal1'].'</td>
						</tr>
						
						<tr>
							<td>Variable Allowance</td>
							<td></td>
							<td>'.$_POST['var_allow'].'</td>
						</tr>
						<tr>
							<td>Leave Travel Allowance</td>
							<td></td>
							<td>'.$_POST['travel_allow'].'</td>
						</tr>
						<tr>
							<td>Personal Allowance</td>
							<td></td>
							<td>'.$_POST['personal_allow'].'</td>
						</tr>
						<tr class="border_none" style="height: 160px;">
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr class="table_seco_inn_table_ftr">
							<td>Total Earnings <small>(Current + Arrears)</small></td>
							<td>&nbsp;</td>
							<td>'.$_POST['total_earning'].'</td>
						</tr>
					</table>
				</td>
				<td>
					<table class="table_seco_inn_table" cellpadding="0" cellspacing="0">
						<tr class="blue">
							<th>Deductions</th>
							<th>Amount <small>(INR)</small></th>
						</tr>
						<tr>
							<td>Provident Fund</td>
							<td>'.$_POST['pf'].'</td>
						</tr>
						<tr>
							<td>Professional Tax</td>
							<td>'.$_POST['pt'].'</td>
						</tr>
						<tr>
							<td>Income Tax</td>
							<td>'.$_POST['it'].'</td>
						</tr>
						<tr class="border_none" style="height:280px">
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr class="table_seco_inn_table_ftr">
							<td>Total Deductions</td>
							<td>'.$_POST['total_deduct'].'</td>
						</tr>

					</table>
				</td>
			</tr>
			
		</table>

		<table class="table_third mar_top table" cellpadding="0" cellspacing="0">
			<td>
				<table cellpadding="0" cellspacing="0">
					<tr>
						<td class="padding_10" colspan="2" style="background: #00a5b4;color: #fff;font-weight: bold;">Retirals<br>as on Month end</td>
						<td>
							<table cellpadding="0" cellspacing="0">
								<tr>
									<td style="background: #00a5b4;color: #fff;">Provident Fund*</td>
								</tr>
								<tr>
									<td>'.$_POST['pro_fund'].'</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
			<td style="width: 3%;">
				
			</td>
			<td>
				<table cellpadding="0" cellspacing="0">
					<tr>
						<td class="padding_10" colspan="2" style="font-weight: bold;background: #00a5b4;color: #fff;">Retirals<br>as on Month end</td>
						<td>
							<table cellpadding="0" cellspacing="0">
								<tr>
									<td style="background: #00a5b4;color: #fff;">Provident Fund*</td>
								</tr>
								<tr>
									<td>'.$_POST['pro_fund1'].'</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
			
		</table>
	  
		<table class="table_fourth table mar_top" cellpadding="0" cellspacing="0">
			<tr class="blue">
				<th colspan="4">Projected Annual Tax Information</th>
				<th colspan="2">Chapter VIA Relief</th>
			</tr>
			<tr>
				<td>Annual Income*</td>
				<td>'.$_POST['anu_income'].'</td>
				<td> Net Tax Income r/o</td>
				<td>'.$_POST['tax_income'].'</td>
				<td>80C-Max 1 Lac</td>
				<td>'.$_POST['max_income'].'</td>
			</tr>
			<tr>
				<td>Professional Tax</td>
				<td>'.$_POST['pro_tax'].'</td>
				<td>Total Tax Payable</td>
				<td>'.$_POST['total_tax'].'</td>
				<td>80D</td>
				<td>'.$_POST['80d'].'</td>
			</tr>
			<tr>
				<td>Chapter VIA relief</td>
				<td>'.$_POST['anu_income'].'</td>
				<td>Tax Deducted till date</td>
				<td>'.$_POST['tax_deduct'].'</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>Balance Tax</td>
				<td>'.$_POST['bal_tax'].'</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			
		</table>

		<table class="table_five table mar_top" cellpadding="0" cellspacing="0">
			<tr class="blue">
				<th colspan="4">Investment Description</th>
				<th colspan="2">Exemption Considered*</th>
			</tr>
			<tr>
				<td style="width: 20%">80D-Medical Premium</td>
				<td style="width: 15%">'.$_POST['medical_pre'].'</td>
				<td style="width: 15%">&nbsp;</td>
				<td style="width: 15%">&nbsp;</td>
				<td>Conveyance</td>
				<td>'.$_POST['convey'].'</td>
			</tr>
			<tr>
				<td>PF Contribution</td>
				<td>'.$_POST['pf_countri'].'</td>
				<td style="width: 15%">&nbsp;</td>
				<td style="width: 15%">&nbsp;</td>
				<td>Rent</td>
				<td>'.$_POST['rent'].'</td>
			</tr>
			<tr>
				<td colspan="6" style="border:0;font-size: 13px;margin-top: 10px;">* Please Note, Annual Income is after considering the above exemption - if any.</td>
			</tr>
		</table></div></div>';


$dompdf->loadHtml($html);
// $dompdf->setPaper('A4', 'landscape');

/* Render the HTML as PDF */
$dompdf->render();


/* Output the generated PDF to Browser */
// $dompdf->stream('codeflies_payslip.pdf');
  $filename = '_Payslip'; // YOU CAN CHOOSE YOUR FILE NAME
  $user_name= str_replace(' ', '_', $_POST["name"]);
  $dompdf->stream($user_name . $filename. ".pdf");

?>
   